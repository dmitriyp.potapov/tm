package tm;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

/**
 * First app ***
 */
public class App 
{
    private boolean             exit;
    private ArrayList<Project>  listProjects;
    private Project             currentProject;

    public App() {
        this.exit       = false;
        listProjects    = new ArrayList<>();
        currentProject  = null;

    }

    public static void main(String[] args )
    {
        new App().go();
    }

    public void go(){

        Scanner in      = new Scanner(System.in);

        while (!exit){
            System.out.println("\nInsert your command in low case:");
            String strIn        = in.nextLine();
            String[] arrCommand = strIn.split(" ");

            chooseCommand(arrCommand);
        }
    }

    private void chooseCommand(String ...arrCommand){
        String cmd = arrCommand[0];

        if ("help".equals(cmd)){
            String help = "There are general commands: \n" +
                    "   help:                               lists all command\n"+
                    "   exit:                               halts the programm\n"+

                    "   create-project <Name>:              creates new project\n" +
                    "   read-projects:                      reads all projects\n" +
                    "   choose-project <Name>:              chooses Nth the project\n" +
                    "   update-project <Name> <New Name>:   update name of the Nth project\n" +
                    "   delete-project <Name>:              deletes the project with name <Name>\n" +
                    "   delete-all-project:                 deletes all project\n" +

                    "   create-task:                        creates new task\n" +
                    "   read-tasks:                         reads tasks for a current project\n" +
                    "   read-all-tasks:                     reads all tasks\n" +
                    "   update-task <Name> <New Name>:      update name of th Nth task\n" +
                    "   delete-task <Name>:                 deletes the task with name <Name> of the cuurrent project\n"+
                    "   delete-tasks:                       deletes all tasks of a current project\n"+
                    "   delete-all-tasks:                   deletes all tasks of all projects\n";
            System.out.println(help);
        }else if ("exit".equals(cmd)){
            System.out.println("Buy-buy....");
            exit = true;
        }else if ("create-project".equals(cmd)){
            if (arrCommand.length > 1){
                currentProject = new Project(arrCommand[1]);
                listProjects.add(currentProject);
                System.out.println("Project [" + currentProject.getName() + "] has created");
            }else
                printNotCorrectCommand();

        }else if ("read-projects".equals(cmd)){
            System.out.println("All projects: \n");
            if (listProjects.size() == 0)
                System.out.println("We have not any project");
            else
                System.out.println(listProjects.toString());

        }else if ("delete-all-project".equals(cmd)){
            listProjects.clear();
            System.out.println("All progects have deleted \n");

        }else if ("delete-project".equals(cmd)){
            if (arrCommand.length > 1){
                Project findProject = null;
                for (Project project : listProjects) {
                    if (project.getName().equals(arrCommand[1])){
                        findProject = project;
                    }
                }

                if (Objects.isNull(findProject)){
                    System.out.println("There isn't any project with name: " + arrCommand[1]);
                }else {
                    listProjects.remove(findProject);
                    System.out.println("Project ["+currentProject.getName()+"] has deleted");
                }

            }else
                printNotCorrectCommand();

        }else if ("choose-project".equals(cmd)){
            if (arrCommand.length > 1){
                Project findProject = null;
                for (Project project : listProjects) {
                    if (project.getName().equals(arrCommand[1])){
                        findProject = project;
                    }
                }

                if (Objects.isNull(findProject)){
                    System.out.println("There isn't any project with name: " + arrCommand[1]);
                    currentProject  = null;
                }else {
                    currentProject  = findProject;
                    System.out.println("Project ["+currentProject.getName()+"] has chosen");
                }
            }else
                printNotCorrectCommand();

        }else if ("update-project".equals(cmd)){
            if (arrCommand.length > 2){
                Project findProject = null;
                for (Project project : listProjects) {
                    if (project.getName().equals(arrCommand[1])){
                        findProject = project;
                    }
                }

                if (Objects.isNull(findProject)){
                    System.out.println("There isn't any project with name: " + arrCommand[1]);
                    currentProject  = null;
                }else {
                    currentProject  = findProject;
                    findProject.setName(arrCommand[2]);
                    System.out.println("The project has updated");
                }
            }else
                printNotCorrectCommand();
        }else if ("create-task".equals(cmd)){
            if (arrCommand.length > 1){
                if (Objects.nonNull(currentProject)){
                    Task newTask = new Task(arrCommand[1]);
                    currentProject.getListTasks().add(newTask);
                    System.out.println("Task [" + newTask.getName() + "] has created in the project ["+ currentProject.getName() +"]");
                }else
                    printNotChoosenCurrentProject();
            }else
                printNotCorrectCommand();

        }else if ("read-tasks".equals(cmd)){
            if (listProjects.size() == 0)
                System.out.println("We have not any project");
            else if (Objects.nonNull(currentProject))
                System.out.println(currentProject.getListTasks());
            else
                printNotCorrectCommand();

        }else if ("read-all-tasks".equals(cmd)){
            for (Project project : listProjects) {
                System.out.println("Project [" + project.getName() + "]:");
                System.out.println(project.getListTasks());
            }
            if (listProjects.size() == 0)
                System.out.println("We have not any project");

        }else if ("delete-tasks".equals(cmd)){
            if (Objects.nonNull(currentProject)){
                currentProject.clearTasks();
            }else
                printNotChoosenCurrentProject();

        }else if ("delete-all-tasks".equals(cmd)){
            for (Project project : listProjects) {
                project.clearTasks();
            }
            System.out.println("All tasks of all projects have deleted");

        }else if ("update-task".equals(cmd)){
            if (arrCommand.length > 2){
                Task findTask = null;
                if (Objects.nonNull(currentProject)){
                    for (Task task : currentProject.getListTasks()) {
                        if (task.getName().equals(arrCommand[1])){
                            findTask = task;
                        }
                    }

                    if (Objects.isNull(findTask)){
                        System.out.println("There isn't any task with name: " + arrCommand[1] + " in the project " + currentProject);
                    }else {
                        findTask.setName(arrCommand[2]);
                        System.out.println("The task has updated");
                    }

                }else
                    printNotChoosenCurrentProject();
            }else
                printNotCorrectCommand();

        }else if ("delete-task".equals(cmd)){
            if (arrCommand.length > 1){
                Task findTask = null;
                if (Objects.nonNull(currentProject)){
                    for (Task task : currentProject.getListTasks()) {
                        if (task.getName().equals(arrCommand[1])){
                            findTask = task;
                        }
                    }

                    if (Objects.isNull(findTask)){
                        System.out.println("There isn't any task with name: " + arrCommand[1] + " in the project " + currentProject);
                    }else {
                        currentProject.remove(findTask);
                        System.out.println("The task has deleted");
                    }

                }else
                    printNotChoosenCurrentProject();
            }else
                printNotCorrectCommand();

        }else if ("".equals(cmd)){
            printNotCorrectCommand();
        }
    }

    private Project findProject(String[] arrCommand){
        Project res = null;

        return res;
    }

    private void printNotCorrectCommand(){
        System.out.println("not correct command, plz try again \n");
    }

    private void printNotChoosenCurrentProject(){
        System.out.println("There isn't any choosen projects! Plz, choose a project!");
    }
}
